package training.jsf.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingJSFContextListener implements ServletContextListener {

    private static final Logger log = LoggerFactory.getLogger(TrainingJSFContextListener.class);
    private static final String STARTUP_MSG = "\n\n-= Starting %s application (ver. %s) =-\n\n";
    private static final String SHUTDOWN_MSG = "\n\n-= Stopping %s application (ver. %s) =-\n\n";
    private static final String APPLICATION_NAME_CONTEXT_PARAM_NAME = "application-name";
    private static final String APPLICATION_VERSION_CONTEXT_PARAM_NAME = "application-version";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log(STARTUP_MSG, sce.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log(SHUTDOWN_MSG, sce.getServletContext());
    }

    private void log(String tpl, ServletContext sc) {
        log.info(String.format(tpl,
                               sc.getInitParameter(APPLICATION_NAME_CONTEXT_PARAM_NAME),
                               sc.getInitParameter(APPLICATION_VERSION_CONTEXT_PARAM_NAME)));
    }
}
