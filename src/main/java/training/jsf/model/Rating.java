package training.jsf.model;

public enum Rating {
	EXCELLENT, OK, BAD;
}
